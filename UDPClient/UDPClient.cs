﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.IO;
using System.Net;



namespace System.Net.Sockets
{ 
    public class Program
    {
        static void Main(string[] args)
        {
            byte[] data = new byte[1024];
            string message, stringData, sname;

            // koneksi
            UdpClient u = new UdpClient();
            IPAddress ipAddress = IPAddress.Parse("192.168.43.168");
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, 9050);
            Socket server = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            // input nama and send
            string name = "";
            Console.Write("Input Name : ");
            name = Console.ReadLine();
            data = Encoding.ASCII.GetBytes(name);
            server.SendTo(data, data.Length, SocketFlags.None, ipEndPoint);

            // sended
            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            EndPoint Remote = (EndPoint)sender;
            data = new byte[1024];


            // recv nama serv
            int recv = server.ReceiveFrom(data, ref Remote);
            sname = Encoding.ASCII.GetString(data, 0, recv);
            Console.WriteLine("My Name Is " + sname);
            

            while (true)
            {
                Console.Write("Me : ");
                // input message 
                message = Console.ReadLine();
                server.SendTo(Encoding.ASCII.GetBytes(message), Remote);
                data = new byte[1024];

                recv = server.ReceiveFrom(data, ref Remote);
                stringData = Encoding.ASCII.GetString(data, 0, recv);
                Console.WriteLine(sname + " : " + stringData);

                if (message == "exit")
                {
                    break;
                }
            }
        }
    }
}

