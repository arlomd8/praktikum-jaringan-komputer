﻿// CLIENT

using System;
using System.Net.Sockets;
using System.IO;
public class ClientSocket1
{
    static void Main(string[] args)
    {
        TcpClient socketForServer;
        bool status = true;
        try
        {
            socketForServer = new TcpClient("192.168.43.168", 8100);
            Console.WriteLine("Connected to Server");
        }
        catch
        {
            Console.WriteLine("Failed to Connect to server{0}:999", "localhost");
            return;
        }
        NetworkStream networkStream = socketForServer.GetStream();
        StreamReader streamreader = new StreamReader(networkStream);
        StreamWriter streamwriter = new StreamWriter(networkStream);
        try
        {
            string clientmessage = "";
            string servermessage = "";
            string serverName = "";
            string clientName = "";


            Console.Write("Input Your Name : ");
            clientName = Console.ReadLine();
            streamwriter.WriteLine(clientName);
            streamwriter.Flush();


            if (socketForServer.Connected)
                {
                    serverName = streamreader.ReadLine();
                Console.WriteLine(serverName + " is sign in!");
                } 


            while (status)
            {
                Console.Write(clientName + " : ");
                clientmessage = Console.ReadLine();
                if ((clientmessage == "bye") || (clientmessage == "BYE") || (servermessage == "bye") || (servermessage == "BYE"))
                {
                    status = false;
                    streamwriter.WriteLine("bye");
                    streamwriter.Flush();
                }
                if ((clientmessage != "bye") && (clientmessage != "BYE"))
                {
                    streamwriter.WriteLine(clientmessage);
                    streamwriter.Flush();
                    servermessage = streamreader.ReadLine();
                    Console.WriteLine(serverName + " : " + servermessage);


                }
            }
        }
        catch
        {
            Console.WriteLine("Exception reading from the server");
        }
        streamreader.Close();
        networkStream.Close();
        streamwriter.Close();
    }
}